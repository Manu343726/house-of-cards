#include <hoc/loglevel.hpp>
#include <hoc/logger.hpp>
#include <unordered_map>

namespace hoc
{

void setLogLevel(const LogLevel level)
{
    hoc::log().setLevel(level);
}

void setLogLevelFromString(const std::string& level)
{
    setLogLevel(logLevelFromString(level));
}

LogLevel logLevelFromString(const std::string& level)
{
    static std::unordered_map<std::string, LogLevel> map = {
        {"TRACE", LogLevel::TRACE},
        {"DEBUG", LogLevel::DEBUG},
        {"INFO", LogLevel::INFO},
        {"WARN", LogLevel::WARN},
        {"ERROR", LogLevel::ERROR},
        {"FATAL", LogLevel::FATAL},
    };

    assert(
        map.count(level) && "hoc::LogLevel enum level string not recognized");
    return map[level];
}
} // namespace hoc

namespace std
{
std::size_t hash<hoc::LogLevel>::operator()(const hoc::LogLevel level) const
{
    using underlying_type = typename std::underlying_type<hoc::LogLevel>::type;
    return std::hash<underlying_type>{}(static_cast<underlying_type>(level));
}
} // namespace std
