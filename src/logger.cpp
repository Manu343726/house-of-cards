#include <hoc/logger.hpp>
#include <hoc/backtrace.hpp>
#include <cassert>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <iostream>

namespace hoc
{

const char* Logger::LOGGER_NAME = "hoc";

Logger::Logger(std::string module) : _module{std::move(module)} {}

spdlog::level::level_enum logLevelToSpdlog(const Logger::Level level)
{
    static std::unordered_map<Logger::Level, spdlog::level::level_enum> map{
        {{Logger::Level::TRACE, spdlog::level::trace},
         {Logger::Level::DEBUG, spdlog::level::debug},
         {Logger::Level::INFO, spdlog::level::info},
         {Logger::Level::WARN, spdlog::level::warn},
         {Logger::Level::ERROR, spdlog::level::err},
         {Logger::Level::FATAL, spdlog::level::critical}}};

    assert(map.count(level) && __FILE__ ": logger level not registered in map");

    return map[level];
}

bool register_logger()
{
    static bool registered = [] {
        try
        {
            spdlog::register_logger(
                spdlog::stdout_color_mt(Logger::LOGGER_NAME));
        }
        catch(const std::exception& ex)
        {
            // check if the logger was initialized previously
            return static_cast<bool>(spdlog::get(Logger::LOGGER_NAME));
        }
        return true;
    }();

    return registered;
}

void check_logger()
{
    bool registered = register_logger();
    (void)registered;
    assert(registered && "ipdiscover logger not registered");
}

void Logger::do_log(const Level level, const std::string& message)
{
    check_logger();

    auto full_message =
        (_module.empty() ? message : fmt::format("[{}] {}", _module, message));
    spdlog::get(LOGGER_NAME)->log(logLevelToSpdlog(level), full_message);
}

void Logger::setLevel(const Logger::Level level)
{
    check_logger();
    spdlog::get(LOGGER_NAME)->set_level(logLevelToSpdlog(level));
}

Logger log(const std::string& module)
{
    return Logger{module};
}

struct log_printer
{
    Logger::Level level;

    template<typename... Args>
    void operator()(Args&&... args) const
    {
        log().log(level, std::forward<Args>(args)...);
    }
};

void dump_backtrace_skipping_frames(
    const Logger::Level level,
    std::size_t         frames_to_skip,
    const std::string&  user_message)
{
    dump_backtrace_skipping_frames(
        log_printer{level}, // GCC 4.8 in C++11 mode has no generic lambdas, we
                            // use this log_printer functor instead
        frames_to_skip + 1 /* skip user frames and this frame */);

    log().log(level, user_message);
}

void dump_backtrace(const Logger::Level level, const std::string& message)
{
    dump_backtrace_skipping_frames(level, 1 /* skip current frame */, message);
}
} // namespace hoc
