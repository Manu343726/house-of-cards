#include <hoc/assert.hpp>
#include <hoc/logger.hpp>

using namespace hoc;

void assert_handler::handle(
    const debug_assert::source_location& loc, const char* expression, void* ptr)
{
    dump_backtrace_skipping_frames(
        Logger::Level::ERROR,
        1 + 3 /* skip this frame and the three ones from debug_assert
                 implementation */
        ,
        "{}:{} (At {:#x}) assertion failed: {}",
        loc.file_name,
        loc.line_number,
        reinterpret_cast<std::uintptr_t>(ptr),
        expression);
}
