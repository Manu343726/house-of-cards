#include <hoc/formatters.hpp>

#ifdef HOC_QT_SUPPORT_ENABLED

std::ostream& operator<<(std::ostream& os, const QString& string)
{
    return os << string.toStdString();
}

std::ostream& operator<<(std::ostream& os, const QHostAddress& address)
{
    return os << address.toString().toStdString();
}

#endif // HOC_QT_SUPPORT_ENABLED
