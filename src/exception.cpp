#include <hoc/exception.hpp>
#include <hoc/backtrace.hpp>
#include <sstream>

using namespace hoc;
using namespace hoc::detail;

struct string_printer
{
    std::ostringstream& ss;

    string_printer(std::ostringstream& ss) : ss(ss) {}

    template<typename... Args>
    void operator()(std::string message, Args&&... args)
    {
        fmt::print(ss, message + "\n", std::forward<Args>(args)...);
    }
};

std::string full_exception_message(std::string message)
{
    std::ostringstream ss;

    dump_backtrace_skipping_frames(
        string_printer(ss),
        5 /* skip this frame and the Exception and ExceptionBase ctors */);

    ss << std::move(message);

    return ss.str();
}

ExceptionBase::ExceptionBase(std::string message)
    : _what{full_exception_message(std::move(message))}
{
}

const char* ExceptionBase::what() const noexcept
{
    return _what.c_str();
}
