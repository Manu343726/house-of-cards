from conans import python_requires
from conans.tools import collect_libs
common = python_requires('conan_common_recipes/0.0.7@Manu343726/testing')

class HouseOfCardsConan(common.CMakePackage):
    name = 'house-of-cards'
    url = 'http://gitlab.by.net/foxter/house-of-cards'
    description = 'Basic utilities (logging, exceptions, stack traces, etc) for the basement of a scalable project'
    version = '0.0.0'
    settings = "os", "compiler", "build_type", "arch"
    options = {'qt_version': ['', '5.9.2', '5.12.0']}
    default_options = {'qt_version': ''}
    scm = {
        'type': 'git',
        'subfolder': name,
        'url': url,
        'revision': 'auto'
    }
    requires = (
        'backward/1.3.1@Manu343726/stable',
        'debug_assert/1.3.3@Manu343726/testing',
        'fmt/5.2.1@bincrafters/stable',
        'spdlog/1.2.1@bincrafters/stable'
    )
    generators = 'cmake', 'cmake_paths', 'cmake_find_package'

    def requirements(self):
        if self.options.qt_version:
            self.requires('qt/{}@bincrafters/stable'.format(self.options.qt_version))

    def package_info(self):
        self.cpp_info.libs = collect_libs(self)

        if self.options.qt_version:
            self.output.info('Packaging with Qt support enabled')

            self.cpp_info.defines = [
                'HOC_QT_SUPPORT_ENABLED',
                'HOC_QT_VERSION=\"{}\"'.format(self.options.qt_version)
            ]

            self.cpp_info.libs.extend(['Qt5Core', 'Qt5Network'])
