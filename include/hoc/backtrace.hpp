#ifndef HOC_BACKTRACE_HPP
#define HOC_BACKTRACE_HPP

#include <backward/backward.hpp>
#include <cstddef>

namespace hoc
{

template<typename Printer>
void dump_backtrace_skipping_frames(Printer printer, std::size_t frames_to_skip)
{
    using namespace backward;
    static constexpr std::size_t MAX_FRAMES = 32;
    StackTrace                   stackTrace;
    TraceResolver                traceResolver;
    stackTrace.load_here(MAX_FRAMES);
    traceResolver.load_stacktrace(stackTrace);

    const std::size_t FRAMES_COUNT = stackTrace.size();

    frames_to_skip +=
        3; // Skip this frame and the backward-cpp implementation ones
    // (On linux these are backward::StackTraceImpl<Linux>::load_here()
    // plus backtrace::detail::unwind())

    for(std::size_t i = 0; i < FRAMES_COUNT - frames_to_skip; ++i)
    {
        const auto& trace =
            traceResolver.resolve(stackTrace[FRAMES_COUNT - i - 1]);

        printer(
            "#{} {}:{} at {}:{}: {}",
            FRAMES_COUNT - frames_to_skip - i,
            trace.object_filename,
            trace.addr,
            trace.source.filename,
            trace.source.line,
            trace.source.function);
    }
}

template<typename Printer>
void dump_backtrace(Printer printer)
{
    dump_backtrace_skipping_frames(printer, 1 /* skip this frame */);
}
} // namespace hoc

#endif // HOC_BACKTRACE_HPP
