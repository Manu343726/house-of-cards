#ifndef HOC_UTILS_HPP
#define HOC_UTILS_HPP

#ifndef HOC_PP_STR
#define HOC_PP_STR_IMPL(x) #x
#define HOC_PP_STR(x) HOC_PP_STR_IMPL(x)
#endif // HOC_UTILS_HPP

#endif // HOC_UTILS_HPP
