#ifndef HOC_LOGLEVEL_HPP
#define HOC_LOGLEVEL_HPP

#include <string>

namespace hoc
{

enum class LogLevel
{
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR,
    FATAL
};

void     setLogLevel(const LogLevel level);
void     setLogLevelFromString(const std::string& level);
LogLevel logLevelFromString(const std::string& level);
} // namespace hoc

namespace std
{
template<>
struct hash<hoc::LogLevel>
{
    std::size_t operator()(const hoc::LogLevel level) const;
};
} // namespace std

#endif // HOC_LOGLEVEL_HPP
