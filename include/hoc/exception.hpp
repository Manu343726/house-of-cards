#ifndef HOC_EXCEPTION_HPP
#define HOC_EXCEPTION_HPP

#include <stdexcept>
#include <string>
#include <hoc/formatters.hpp>
#include <hoc/utils.hpp>

namespace hoc
{

namespace detail
{

class ExceptionBase : public std::exception
{
public:
    template<typename... Args>
    ExceptionBase(
        const char*        exception_class_name,
        const std::string& message,
        Args&&... args)
        : ExceptionBase{fmt::format(
              "({}) " + message,
              exception_class_name,
              std::forward<Args>(args)...)}
    {
    }

    const char* what() const noexcept override final;

private:
    ExceptionBase(std::string message);
    std::string _what;
};
} // namespace detail

template<typename Derived>
struct Exception : public hoc::detail::ExceptionBase
{
    template<typename... Args>
    Exception(const std::string& message, Args&&... args)
        : hoc::detail::ExceptionBase{Derived::exception_class_name(),
                                     message,
                                     std::forward<Args>(args)...}
    {
    }
};
} // namespace hoc

#define HOC_DECLARE_EXCEPTION(ex)                           \
    struct ex : public ::hoc::Exception<ex>                 \
    {                                                       \
        using ::hoc::Exception<ex>::Exception;              \
                                                            \
        static constexpr const char* exception_class_name() \
        {                                                   \
            return HOC_PP_STR(ex);                          \
        }                                                   \
    }

#define HOC_NOTHROW(...)       \
    [&] {                      \
        try                    \
        {                      \
            (void)__VA_ARGS__; \
            return true;       \
        }                      \
        catch(...)             \
        {                      \
            return false;      \
        }                      \
    }()

#endif // HOC_EXCEPTION_HPP
