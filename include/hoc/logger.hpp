#ifndef HOC_LOGGER_HPP
#define HOC_LOGGER_HPP

#include <string>
#include <functional>
#include <hoc/formatters.hpp>
#include <hoc/loglevel.hpp>

namespace hoc
{

class Logger
{
public:
    Logger(std::string module);

    using Level = hoc::LogLevel;

    void setLevel(const Level level);

    template<typename... Args>
    void trace(const std::string& message, Args&&... args)
    {
        log(Level::TRACE, message, std::forward<Args>(args)...);
    }
    template<typename... Args>
    void debug(const std::string& message, Args&&... args)
    {
        log(Level::DEBUG, message, std::forward<Args>(args)...);
    }
    template<typename... Args>
    void info(const std::string& message, Args&&... args)
    {
        log(Level::INFO, message, std::forward<Args>(args)...);
    }
    template<typename... Args>
    void warn(const std::string& message, Args&&... args)
    {
        log(Level::WARN, message, std::forward<Args>(args)...);
    }
    template<typename... Args>
    void error(const std::string& message, Args&&... args)
    {
        log(Level::ERROR, message, std::forward<Args>(args)...);
    }
    template<typename... Args>
    void fatal(const std::string& message, Args&&... args)
    {
        log(Level::FATAL, message, std::forward<Args>(args)...);
    }

    template<typename... Args>
    void log(const Level level, const std::string& message, Args&&... args)
    {
        do_log(level, fmt::format(message, std::forward<Args>(args)...));
    }

    static const char* LOGGER_NAME;

private:
    void do_log(const Level level, const std::string& message);

    std::string _module;
};

Logger log(const std::string& module = "");

void dump_backtrace_skipping_frames(
    const Logger::Level level,
    std::size_t         frames_to_skip,
    const std::string&  user_message);

void dump_backtrace(const Logger::Level level, const std::string& user_message);

template<typename... Args>
void dump_backtrace_skipping_frames(
    const Logger::Level log_level,
    const std::size_t   frames_to_skip,
    const std::string&  message,
    Args&&... args)
{
    dump_backtrace_skipping_frames(
        log_level,
        frames_to_skip,
        fmt::format(message, std::forward<Args>(args)...));
}

template<typename... Args>
void dump_backtrace(
    const Logger::Level log_level, const std::string& message, Args&&... args)
{
    dump_backtrace_skipping_frames(
        log_level,
        1 /* skip this frame */,
        fmt::format(message, std::forward<Args>(args)...));
}
} // namespace hoc

#endif // HOC_LOGGER_HPP
