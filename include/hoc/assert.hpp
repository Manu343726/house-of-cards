#ifndef HOC_ASSERT_HPP
#define HOC_ASSERT_HPP

#include <debug_assert.hpp>

#ifndef HOC_ASSERT_LEVEL
#ifdef NDEBUG
#define HOC_ASSERT_LEVEL 0
#else
#define HOC_ASSERT_LEVEL 1
#endif // NDEBUG
#endif // HOC_ASSERT_LEVEL

namespace hoc
{

struct assert_handler : debug_assert::set_level<HOC_ASSERT_LEVEL>
{
    static void handle(
        const debug_assert::source_location& loc,
        const char*                          expression,
        void*                                ptr = nullptr);
};
} // namespace hoc

#endif // HOC_ASSERT_HPP
