#ifndef HOC_FORMATTERS_HPP
#define HOC_FORMATTERS_HPP

/*
 * Enable fmt formatting of Qt types for easy logging
 * See http://fmtlib.net/latest/api.html#formatting-user-defined-types
 * to understand the formatters
 */

#ifdef HOC_QT_SUPPORT_ENABLED
#include <QtNetwork/QHostAddress>
#include <QtCore/QString>
#endif // HOC_QT_SUPPORT_ENABLED
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <string>
#include <type_traits>

/*
 * For some reason GCC 4.8 does not accept the fmt formatter specialization
 * (clang 4.0 does). Instead, we overload operator<<(std::ostream&) operators.
 * Note this solution may conflict with libraries that also overload these
 * operators
 */
#ifndef HOC_FORMATTERS_OVERLOAD_OSTREAM_OPERATORS
#if defined(__GNUC__) && !defined(__clang__) && __GNUC__ < 5
#define HOC_FORMATTERS_OVERLOAD_OSTREAM_OPERATORS
#endif // GCC 5.x.y
#endif // HOC_FORMATTERS_OVERLOAD_OSTREAM_OPERATORS

#ifdef HOC_FORMATTERS_OVERLOAD_OSTREAM_OPERATORS
#ifdef HOC_QT_SUPPORT_ENABLED
std::ostream& operator<<(std::ostream& os, const QString& string);
std::ostream& operator<<(std::ostream& os, const QHostAddress& address);
#endif // HOC_QT_SUPPORT_ENABLED
#else  // HOC_FORMATTERS_OVERLOAD_OSTREAM_OPERATORS

#ifdef HOC_QT_SUPPORT_ENABLED
namespace fmt
{
template<>
struct formatter<QString> : formatter<std::string>
{
    template<typename FormatContext>
    auto format(const QString& string, FormatContext& ctx)
        -> decltype(formatter<std::string>::format(
            std::declval<std::string>(), std::declval<FormatContext&>()))
    {
        return formatter<std::string>::format(string.toStdString(), ctx);
    }
};

template<>
struct formatter<QHostAddress> : formatter<QString>
{
    template<typename FormatContext>
    auto format(const QHostAddress& address, FormatContext& ctx)
        -> decltype(formatter<QString>::format(
            std::declval<QString>(), std::declval<FormatContext&>()))
    {
        return formatter<QString>::format(address.toString(), ctx);
    }
};
} // namespace fmt
#endif // HOC_QT_SUPPORT_ENABLED
#endif // HOC_FORMATTERS_OVERLOAD_OSTREAM_OPERATORS

#endif // HOC_FORMATTERS_HPP
