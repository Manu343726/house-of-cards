#!/bin/bash
set -e
set -x

mkdir -p build && cd build
conan install ..
cmake .. -G Ninja -DHOUSE_OF_CARDS_BUILD_TESTS=ON
cmake --build .

if $CXX -dumpmachine | grep arm; then
    echo building for ARM target, skipping tests!
else
    ctest . -V
fi
