#!/bin/bash
set -e
set -x

sudo pip install --upgrade conan
conan remote add manu343726 https://api.bintray.com/conan/manu343726/conan-packages

if [[ -e "$CONAN_MANU343726_API_KEY" ]]; then
    conan user $CONAN_MANU343726_USER -p $CONAN_MANU343726_API_KEY -r by
else
    echo conan credentials not found
fi
