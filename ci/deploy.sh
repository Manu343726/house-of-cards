#!/bin/bash
set -e
set -x

rm -rf build
conan create . bytech/testing

if [[ -e "$CONAN_MANU343726_API_KEY" ]]; then
    conan upload house-of-cards -r manu343726 --all --confirm --force
else
    echo conan credentials not found, deploy aborted
fi
